﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] float jumpForce = 5f;
    [SerializeField] bool isGrounded = false;

    private Rigidbody2D rigidBody;
    private float timeSinceLastJump = Mathf.Infinity;
    private float timeBetweenJumps = 1.5f;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        HandleMovement();
    }

    private void HandleMovement()
    {
        float horizontalMovement = Input.GetAxisRaw("Horizontal");
        rigidBody.velocity = new Vector2(horizontalMovement, rigidBody.velocity.y);
        timeSinceLastJump += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            rigidBody.velocity = new Vector2(rigidBody.velocity.x, jumpForce);
            isGrounded = false;
            timeSinceLastJump = 0;
        }        

        if (timeSinceLastJump > timeBetweenJumps)
        {
            isGrounded = true;
        }
    }
}
